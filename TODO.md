### Changes to implement in future versions of Poncho App

##### Improve my testing skills
- [ ] Write unit tests

##### Improve code style 
- [ ] Reduce size of MakeWeatherApiCall() & userRequestSwitch() functions

##### Practice working with other devs 
- [ ] Ask for feedback on code order 
